#include <stdio.h>
float input();
float volume(float,float,float);
void output(float);
int main()
{
   
    float h,d,b,v;
    h=input();
    d=input();
    b=input();
    v=volume(h,d,b);
    output(v);
    return 0;
}
float input()
{
   float a;
   printf("Enter the dimensions of tromboloid");
   scanf("%f",&a);
   return a;
}

float volume(float h,float d,float b)
{
    float volume;
    volume = 1.0/3*((h*d) + d)/b;
    return volume;
    
}

void output(float v)
{
    printf("Volume of tromboloid is %f",v);
}