//Write a program to add two user input numbers using one function.
#include<stdio.h>
int main()
{
int n1, n2, sum;
printf("enter two numbers");
scanf("%d %d", &n1,&n2);
sum = n1 + n2;
printf( "Sum of %d + %d = %d ", n1, n2, sum);
return 0;
}