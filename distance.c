/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, PHP, Ruby, 
C#, VB, Perl, Swift, Prolog, Javascript, Pascal, HTML, CSS, JS
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include<stdio.h>
#include<math.h>
float input(char,int);
float distance(float,float,float,float);
void output(float,float,float,float,float);
int main()
{
    float x1,y1,x2,y2,dist;
    x1=input('x',1);
    y1=input('y',1);
    x2=input('x',2);
    y2=input('y',2);
    dist=distance(x1,y1,x2,y2);
    output(x1,y1,x2,y2,dist);
    return 0;
    
}
float input(char i,int j)
{
    float n;
    printf("enter the %c%d  coordinates of point ",i,j);
    scanf("%f",&n);
    return n;
}
float distance(float x1, float y1, float x2, float y2)
{
    float distance = sqrt(pow((x2-x1),2)+pow((y2-y1),2));
    return distance;
}
void output(float x1, float x2, float y1, float y2, float dist)
{
    printf("the distance between %.2f,%.2f and %.2f,%.2f is: %.2f\n",x1,y1,x2,y2,dist);
}
