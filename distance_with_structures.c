//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h>
#include<math.h>
struct Point
{
    float x, y;
};
float distance(struct Point a, struct Point b)
{
    float dis;
    dis= sqrt(pow((a.x - b.x),2) + pow((a.y - b.y),2));
    return dis;
}
int main()
{
    struct Point a,b;
    float dis;
    printf("Enter coordinates of point a:");
    scanf("%f%f", &a.x, &a.y);
    printf("Enter coordinate of point b:");
    scanf("%f%f",&b.x,&b.y);
    printf("distance between a and b is : %.2f\n", distance(a,b));
    return 0;
}
